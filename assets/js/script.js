const container = document.querySelector(".container");
const clockContainer = document.getElementById("clock-container");
const clock = document.querySelector("#clock");
const formatBtn = document.querySelector(".format > button");
const colorButtons = document.querySelectorAll(".colorButtons");
const fontButtons = document.querySelectorAll(".fontButtons");

let status = true;

formatBtn.addEventListener("click", function () {
    status = !status;
});

colorButtons.forEach(function (element) {
    element.addEventListener("click", function () {
        clock.style.color = this.id;
    });
});

fontButtons.forEach(function (element) {
    element.addEventListener("click", function () {
        clock.style.fontFamily = this.innerHTML;
    });
});



function twelveHourFormat() {
    let d = new Date();
    let time = d.toLocaleTimeString();

    clock.innerHTML = `${time}`;
}

function twentyFourHourFormat() {
    let d = new Date();
    let hours = putZero(d.getHours());
    let mins = putZero(d.getMinutes());
    let secs = putZero(d.getSeconds());

    clock.innerHTML = `${hours}:${mins}:${secs}`;
}

function putZero(time) {
    if(time < 10) {
        return `0${time}`;
    } else {
        return time;
    }
}

function digitalClock() {
    if(status) {
        formatBtn.innerHTML = `12-Hour Format`;
        return twentyFourHourFormat();
    } else {
        formatBtn.innerHTML = `24-Hour Format`;
        return twelveHourFormat();
    }

}

// setInterval(twelveHourFormat, 1000);
// setInterval(twentyFourHourFormat, 1000);
setInterval(digitalClock, 1000);
